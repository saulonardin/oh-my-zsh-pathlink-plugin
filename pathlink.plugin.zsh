
export PATHLINK_PATH_ORIGINAL=$${PATHLINK_BKP:$PATH}

PATH_LINK_HOME=${PATH_LINK_HOME:-$HOME/.pathlink}

pathlinkUpdate(){
    if [[ -e $PATH_LINK_HOME ]]; then

    	_PATH_LINK=$(
            find $PATH_LINK_HOME -type l -exec basename {} \; \
            | sort -n \
            | sed -e "s|^|${PATH_LINK_HOME}/|" \
            | xargs readlink 2>/dev/null \
            | paste -sd ':' -
        )

    	if [ ! -z "$_PATH_LINK" ]; then
    		export PATH="$_PATH_LINK:$PATHLINK_PATH_ORIGINAL"
    	fi
    fi
}

pathlinkUpdate
