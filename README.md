Oh-my-zsh-PathLink
==================

Easiest way to add folders to you `PATH` variable

This Oh-my-zsh plugin add all symbolic links under `~/.pathlink` folder to the
begin of your `PATH` variable*.*


Installation
============

Add path link to your `˜/.zshrc` file:

*plugins=( pathlink git aws …)*

```bash
mkdir -p ~/.pathlink && \
git clone https://bitbucket.org/saulonardin/oh-my-zsh-pathlink-plugin.git ~/.oh-my-zsh/plugins/pathlink
```
 
